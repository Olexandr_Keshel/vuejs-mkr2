// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDXLYVeG-aBA3_pn9psGrqLngc7y1Zmwqk",
  authDomain: "mkr2-11504.firebaseapp.com",
  projectId: "mkr2-11504",
  storageBucket: "mkr2-11504.appspot.com",
  messagingSenderId: "1088283804162",
  appId: "1:1088283804162:web:d5142fc63eb77ee0f1dd1b"
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)

const db = getFirestore(app)

export default db