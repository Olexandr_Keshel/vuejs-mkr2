import { createRouter, createWebHistory } from 'vue-router'
import ViewProducts from '../views/ViewProducts.vue'
import CreateProduct from '../views/CreateProduct.vue'
import ViewOneProduct from '../views/ViewOneProduct.vue'

import About from '../views/About.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: ViewProducts
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/new',
    name: 'new-product',
    component: CreateProduct
  },
  {
    path: '/:id',
    name: 'view',
    component: ViewOneProduct
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
